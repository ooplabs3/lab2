package ru.nsu.ccfit.sukhinsky;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipManager;
import ru.nsu.ccfit.sukhinsky.gui.FrameManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import static java.lang.Thread.sleep;

public class Controller implements ShipManager.Subscriber {
    private int FPS;
    private ShipManager manager;
    private FrameManager frameManager;
    private Timer timer;
    private NewGameActionListener newGameActionListener;
    private ExitActionListener exitActonListener;
    private CAdapter cKeyAdapter;
    private TimerActionListener timerActionListener;

    public Controller(ShipManager manager, int FPS) {
        this.FPS = FPS;
        this.manager = manager;
        manager.subscribeLanded(this);

        timerActionListener = new TimerActionListener();
        newGameActionListener = new NewGameActionListener();
        exitActonListener =  new ExitActionListener();
        timer = new Timer(1000/FPS, timerActionListener);
        cKeyAdapter = new CAdapter();
        this.frameManager = new FrameManager(manager, manager.getShipData(), cKeyAdapter, newGameActionListener, exitActonListener);
        this.frameManager.addKeyListener(cKeyAdapter);
    }

    @Override
    public void notify(ShipManager.ModelEvent event) {
        if (event == ShipManager.ModelEvent.LANDED) {
            landed();
        }
    }

    private void landed() {
        timer.stop();
    }

    private class NewGameActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            frameManager.newGame(cKeyAdapter);
            manager.update(Command.RESTART);
            timer.start();
        }
    }
    private class ExitActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    private class TimerActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            manager.update(Command.REFRESH);
        }
    }

    private class CAdapter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {

            int key = e.getKeyCode();

            switch (key) {

                case KeyEvent.VK_W:
                    manager.update(Command.ENGINE_ON);
                    break;

                case KeyEvent.VK_SHIFT:
                    manager.update(Command.ENGINE_FORCE_ON);
                    break;

                case KeyEvent.VK_D:
                    manager.update(Command.ROTATION_LEFT);
                    break;

                case KeyEvent.VK_A:
                    manager.update(Command.ROTATION_RIGHT);
                    break;

                default:
                    break;
            }

        }
        @Override
        public void keyReleased(KeyEvent e) {
            int key = e.getKeyCode();

            switch (key) {

                case KeyEvent.VK_W:
                    manager.update(Command.ENGINE_OFF);
                    break;

                case KeyEvent.VK_SHIFT:
                    manager.update(Command.ENGINE_FORCE_OFF);
                    break;

                case KeyEvent.VK_D:
                    manager.update(Command.ROTATION_OFF);
                    break;

                case KeyEvent.VK_A:
                    manager.update(Command.ROTATION_OFF);
                    break;

                default:
                    break;
            }
        }
    }
}
