package ru.nsu.ccfit.sukhinsky;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;
import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipManager;

import java.awt.*;

import static java.lang.Thread.sleep;

public class Main {

    private static void writeShipData(ShipData shipData) { // TODO: Remove it
        System.out.println("X: " + shipData.getCoords().getX());
        System.out.println("Y: " + shipData.getCoords().getY());
        System.out.println("SpeedX: " + shipData.getSpeed().getSpeedX());
        System.out.println("SpeedY: " + shipData.getSpeed().getSpeedY());
        System.out.println("Heat: " + shipData.getEngineHeat());
        System.out.println("Is overheated?: " + shipData.isEngineOverheated());
        System.out.println("Is engine on?: " + shipData.isEngineOn());
        System.out.println();
    }
    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {

            int FPS = 60;
            ShipManager shipManager = new ShipManager(new ShipData(),0.05 / FPS, FPS);

            Controller controller = new Controller(shipManager, FPS);
        });

}
}