package ru.nsu.ccfit.sukhinsky;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipManager;

public interface ModelPublisher {
    void subscribeUpdate(ShipManager.Subscriber subscriber);
    void subscribeLanded(ShipManager.Subscriber subscriber);
}
