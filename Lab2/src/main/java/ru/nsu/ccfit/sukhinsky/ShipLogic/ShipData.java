package ru.nsu.ccfit.sukhinsky.ShipLogic;

import ru.nsu.ccfit.sukhinsky.Coordinates;
import ru.nsu.ccfit.sukhinsky.Rotation;
import ru.nsu.ccfit.sukhinsky.Speed;

public class ShipData {
    private double angle;
    private Coordinates coords;
    private double enginePower;
    private boolean engineOn;
    private double engineHeat;
    private boolean engineOverheated;
    private double angleOffset;
    private Speed speed;
    private Rotation rotation;
    private double rotationSpeed;
    private boolean engineForced;
    private double minimumHeat;
    private double maximumHeat;
    private double heatingSpeed;
    private double forcedEnginePowerMultiplier;
    private double forcedEngineHeatingSpeedMultiplier;
    private double engineCoolingSpeedMultiplier;
    private boolean landed;
    private boolean crushed;
    private double maximumSafeSpeed;

    private void shipInit() {
        this.angle = 90;
        this.coords = new Coordinates(0, 1000);
        this.enginePower = 0.06 / 60.0;
        this.engineOn = false;
        this.engineHeat = 0;
        this.engineOverheated = false;
        this.angleOffset = 5;
        this.speed = new Speed(0, 0);
        this.rotation = Rotation.NO;
        this.rotationSpeed = 360 * 0.06 / 60;
        this.engineForced = false;
        this.maximumHeat = 100;
        this.heatingSpeed = 5 / 60.0;
        this.forcedEnginePowerMultiplier = 3;
        this.forcedEngineHeatingSpeedMultiplier = 2;
        this.engineCoolingSpeedMultiplier = 2;
        this.maximumSafeSpeed = 8/60.0;
    }

    public ShipData() {}

    public double getAngle() {
        return angle;
    }

    protected void setAngle(double angle) {
        this.angle = angle;
    }

    public Coordinates getCoords() {
        return coords;
    }

    protected void setCoords(Coordinates coords) {
        this.coords = coords;
    }

    public double getEnginePower() {
        return enginePower;
    }

    protected void setEnginePower(double enginePower) {
        this.enginePower = enginePower;
    }

    public boolean isEngineOn() {
        return engineOn;
    }

    protected void setEngineOn(boolean engineOn) {
        this.engineOn = engineOn;
    }

    public double getEngineHeat() {
        return engineHeat;
    }

    protected void setEngineHeat(double engineHeat) {
        this.engineHeat = engineHeat;
    }

    public boolean isEngineOverheated() {
        return engineOverheated;
    }

    protected void setEngineOverheated(boolean engineOverheated) {
        this.engineOverheated = engineOverheated;
    }

    public double getAngleOffset() {
        return angleOffset;
    }

    protected void setAngleOffset(double angleOffset) {
        this.angleOffset = angleOffset;
    }

    public Speed getSpeed() {
        return speed;
    }

    protected void setSpeed(Speed speed) {
        this.speed = speed;
    }

    public Rotation getRotation() {
        return rotation;
    }

    protected void setRotation(Rotation rotation) {
        this.rotation = rotation;
    }

    public double getRotationSpeed() {
        return rotationSpeed;
    }

    protected void setRotationSpeed(double rotationSpeed) {
        this.rotationSpeed = rotationSpeed;
    }

    public boolean isEngineForced() {
        return engineForced;
    }

    protected void setEngineForced(boolean engineForced) {
        this.engineForced = engineForced;
    }

    public double getMaximumHeat() {
        return maximumHeat;
    }

    protected void setMaximumHeat(double maximumHeat) {
        this.maximumHeat = maximumHeat;
    }

    public double getHeatingSpeed() {
        return heatingSpeed;
    }

    protected void setHeatingSpeed(double heatingSpeed) {
        this.heatingSpeed = heatingSpeed;
    }

    public double getForcedEnginePowerMultiplier() {
        return forcedEnginePowerMultiplier;
    }

    protected void setForcedEnginePowerMultiplier(double forcedEnginePowerMultiplier) {
        this.forcedEnginePowerMultiplier = forcedEnginePowerMultiplier;
    }

    public double getForcedEngineHeatingSpeedMultiplier() {
        return forcedEngineHeatingSpeedMultiplier;
    }

    protected void setForcedEngineHeatingSpeedMultiplier(double forcedEngineHeatingSpeedMultiplier) {
        this.forcedEngineHeatingSpeedMultiplier = forcedEngineHeatingSpeedMultiplier;
    }

    public double getEngineCoolingSpeedMultiplier() {
        return engineCoolingSpeedMultiplier;
    }

    protected void setEngineCoolingSpeedMultiplier(double engineCoolingSpeedMultiplier) {
        this.engineCoolingSpeedMultiplier = engineCoolingSpeedMultiplier;
    }

    public double getMinimumHeat() {
        return minimumHeat;
    }

    protected void setMinimumHeat(double minimumHeat) {
        this.minimumHeat = minimumHeat;
    }

    public boolean isLanded() {
        return landed;
    }

    protected void setLanded(boolean landed) {
        this.landed = landed;
    }

    public boolean isCrushed() {
        return crushed;
    }

    protected void setCrushed(boolean crushed) {
        this.crushed = crushed;
    }

    public double getMaximumSafeSpeed() {
        return maximumSafeSpeed;
    }

    protected void setMaximumSafeSpeed(double maximumSafeSpeed) {
        this.maximumSafeSpeed = maximumSafeSpeed;
    }
}


