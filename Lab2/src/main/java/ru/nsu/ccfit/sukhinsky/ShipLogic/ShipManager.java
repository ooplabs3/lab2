package ru.nsu.ccfit.sukhinsky.ShipLogic;

import ru.nsu.ccfit.sukhinsky.*;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.*;

public class ShipManager implements ModelPublisher {
    private ShipData shipData;
    private double gravity;
    private List<Subscriber> updateSubscribers;
    private List<Subscriber> landedSubscribers;
    private int FPS;

    public ShipManager(ShipData shipData, double gravity, int FPS) {
        this.shipData = shipData;
        this.gravity = gravity;
        updateSubscribers = new ArrayList<>();
        landedSubscribers = new ArrayList<>();
        this.FPS = FPS;
        resetShip();
    }

    public ShipData getShipData() {
        return shipData;
    }

    public void setShipData(ShipData shipData) {
        this.shipData = shipData;
        notifyUpdateSubscribers();
    }

    public double getGravity() {
        return gravity;
    }

    public void setGravity(double gravity) {
        this.gravity = gravity;
        notifyUpdateSubscribers();
    }

    private void recountHeat() {
        double heatingSpeerMultiplier;
        double newHeat = shipData.getEngineHeat();
        if (!shipData.isEngineOverheated()) {
            if (shipData.isEngineOn()) {
                if (shipData.isEngineForced()) {
                    heatingSpeerMultiplier = shipData.getForcedEngineHeatingSpeedMultiplier();
                }
                else {
                    heatingSpeerMultiplier = 1;
                }
                newHeat += shipData.getHeatingSpeed() * heatingSpeerMultiplier;
                if (newHeat >= shipData.getMaximumHeat()) {
                    newHeat = shipData.getMaximumHeat();
                    shipData.setEngineOverheated(true);
                }
            }
        }
        if (shipData.isEngineOverheated() || !shipData.isEngineOn()) {
            newHeat -= shipData.getHeatingSpeed() * shipData.getEngineCoolingSpeedMultiplier();
            if (newHeat <= shipData.getMinimumHeat()) {
                newHeat = shipData.getMinimumHeat();
                shipData.setEngineOverheated(false);
            }
        }
        shipData.setEngineHeat(newHeat);
    }
    private void recountCoordinates() {
        Coordinates newCoords = shipData.getCoords();
        newCoords.setX(shipData.getCoords().getX() + shipData.getSpeed().getSpeedX());
        newCoords.setY(shipData.getCoords().getY() + shipData.getSpeed().getSpeedY());
        shipData.setCoords(newCoords);
    }
    private void recountSpeed() {
        double accelerationX, accelerationY, power;

        if (!shipData.isEngineOn() || shipData.isEngineOverheated()) {
            power = 0;
        }
        else {
            power = shipData.getEnginePower();
        }

        if (shipData.isEngineForced()) {
            power *= shipData.getForcedEnginePowerMultiplier();
        }

        accelerationX = cos(Math.toRadians(shipData.getAngle())) * power;
        accelerationY = sin(Math.toRadians(shipData.getAngle())) * power - gravity;

        Speed newSpeed = shipData.getSpeed();
        newSpeed.setSpeedX(newSpeed.getSpeedX() + accelerationX);
        newSpeed.setSpeedY(newSpeed.getSpeedY() + accelerationY);
        shipData.setSpeed(newSpeed);
     }

    private void recountAngle() {
        double newAngle;
        double oldAngle = shipData.getAngle();
        switch (shipData.getRotation()) {
            case NO:
                newAngle = oldAngle;
                break;
            case LEFT:
                newAngle = oldAngle + shipData.getRotationSpeed();
                break;
            case RIGHT:
                newAngle = oldAngle - shipData.getRotationSpeed();
                break;
            default:
                newAngle = oldAngle;
                break;
        }
        shipData.setAngle(newAngle);
    }
    private void checkLanding() {
        if (shipData.getCoords().getY() > 0) {
            shipData.setLanded(false);
            return;
        }
        shipData.setLanded(true);
        if (abs(shipData.getSpeed().getSpeedY()) > shipData.getMaximumSafeSpeed()) {
            shipData.setCrushed(true);
        }

        shipData.setCoords(new Coordinates(shipData.getCoords().getX(), 0));
        shipData.setSpeed(new Speed(0, 0));
        notifyLandedSubscribers();
    }

    private void refresh() {
        checkLanding();
        recountAngle();
        recountSpeed();
        recountCoordinates();
        recountHeat();
        notifyUpdateSubscribers();
    }
    private void switchEngine(boolean isEngineOn) {
        shipData.setEngineOn(isEngineOn);
    }
    private void changeRotation(Rotation rotation) {
        shipData.setRotation(rotation);
    }
    private void changeEngineForce(boolean isForced) {
        shipData.setEngineForced(isForced);
    }

    public void update(Command command) {
        switch (command){
            case REFRESH:
                refresh();
                break;
            case ENGINE_ON:
                switchEngine(true);
                break;
            case ENGINE_OFF:
                switchEngine(false);
                break;
            case ROTATION_OFF:
                changeRotation(Rotation.NO);
                break;
            case ROTATION_LEFT:
                changeRotation(Rotation.LEFT);
                break;
            case ROTATION_RIGHT:
                changeRotation(Rotation.RIGHT);
                break;
            case ENGINE_FORCE_ON:
                changeEngineForce(true);
                break;
            case ENGINE_FORCE_OFF:
                changeEngineForce(false);
                break;
            case RESTART:
                resetShip();
            default:
                break;
        }
        notifyUpdateSubscribers();
    }
    public enum ModelEvent {
        UPDATE, LANDED
    }
    public interface Subscriber {
        void notify(ModelEvent event);
    }

    private void notifyUpdateSubscribers() {
        for (Subscriber subscriber: updateSubscribers) {
            subscriber.notify(ModelEvent.UPDATE);
        }
    }
    private void notifyLandedSubscribers() {
        for (Subscriber subscriber: landedSubscribers) {
            subscriber.notify(ModelEvent.LANDED);
        }
    }

    @Override
    public void subscribeUpdate(Subscriber subscriber) {
        updateSubscribers.add(subscriber);
    }
    @Override
    public void subscribeLanded(Subscriber subscriber) {
        landedSubscribers.add(subscriber);
    }
    private void resetShip() {
        shipData.setAngle(90);
        shipData.setCoords(new Coordinates(0, 1000));
        shipData.setEnginePower(0.06 / FPS);
        shipData.setEngineOn(false);
        shipData.setEngineOverheated(false);
        shipData.setEngineHeat(0);
        shipData.setAngleOffset(5);
        shipData.setSpeed(new Speed(0, 0));
        shipData.setRotation(Rotation.NO);
        shipData.setRotationSpeed(360 * 0.06 / FPS);
        shipData.setEngineForced(false);
        shipData.setMaximumHeat(100);
        shipData.setHeatingSpeed(5.0 / FPS);
        shipData.setEngineCoolingSpeedMultiplier(2);
        shipData.setForcedEnginePowerMultiplier(3);
        shipData.setForcedEngineHeatingSpeedMultiplier(2);
        shipData.setMaximumSafeSpeed(8.0 / FPS);

    }
}
