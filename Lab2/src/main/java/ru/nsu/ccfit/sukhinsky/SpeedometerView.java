package ru.nsu.ccfit.sukhinsky;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;

import java.awt.*;

public class SpeedometerView {
    private int x;
    private int y;
    private Font font;
    private int fontSize;
    private int screenHeight;
    private int screenWidth;
    private int meterHeight;
    private int meterWidth;
    private ShipData shipData;
    private Stroke stroke;
    private int strokeSize;
    private Color color;

    public void draw(Graphics g) {
        drawMeter(g);
    }

    private void drawMeter(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(new Color(255, 255, 255));
        g2.fillRect(x, y, meterWidth, meterHeight);
        g2.setColor(color);
        g2.setStroke(new BasicStroke(10));
        g2.drawRect(x, y, meterWidth, meterHeight);
        String speedText = String.format("%.0f", shipData.getSpeed().getSpeedY() * 60);
        FontMetrics fontMetrics = g2.getFontMetrics(font);
        int textX = x + meterWidth / 2 - fontMetrics.stringWidth(speedText) / 2;
        int textY = y + meterHeight / 2 + fontSize / 2;

        g2.drawString(speedText, textX, textY);
    }

    public SpeedometerView(ShipData shipData, int x, int y, int screenHeight, int screenWidth) {
        this.shipData = shipData;
        this.x = x;
        this.y = y;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        meterHeight = screenHeight / 15;
        meterWidth = screenWidth / 10;
        font = new Font(Font.SANS_SERIF, Font.BOLD, screenWidth / 50);
        fontSize = screenWidth / 50;
        color = new Color(50, 180, 110);

        strokeSize = 10;
        stroke = new BasicStroke(strokeSize);
    }

}
