package ru.nsu.ccfit.sukhinsky.gui;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;
import ru.nsu.ccfit.sukhinsky.Speed;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class CloudView {
    private int gap;
    private int x;
    private int y;
    private ShipData shipData;
    private ImageIcon cloudImage;
    private int screenHeight;
    private int screenWidth;
    private boolean invisible;
    private Random random;

    public CloudView(ShipData shipData, ImageIcon cloudImage, int screenHeight, int screenWidth, int seed) {
        gap = screenHeight;
        random = new Random(seed);
        this.cloudImage = cloudImage;
        this.shipData = shipData;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        x = random.nextInt(screenWidth + gap);
        y = random.nextInt(screenHeight + gap);
        invisible = false;
    }

    public void draw(Graphics g) {
        recountPosition();
        if (!invisible) {
            g.drawImage(cloudImage.getImage(), x, y, null);
        }

    }
    private void recountPosition() {
        Speed shipSpeed = shipData.getSpeed();
        x -= (shipSpeed.getSpeedX() * 60);
        if ((x < 0 - gap) || (x > screenWidth + gap)) {
            resetCoordinates();
        }

        y += (shipSpeed.getSpeedY() * 60);
        if ((y < 0 - gap) || (y > screenHeight + gap)) {
            resetCoordinates();
        }
    }
    private void resetCoordinates() {
        if (shipData.getCoords().getY() < 150) {
            invisible = true;
            return;
        }
        invisible = false;
        if ((x < 0 - gap) || (x > screenWidth + gap)) {
            if (x > 0) {
                x = -gap;
                y = random.nextInt(screenHeight + gap) - gap/2;
            }
            else {
                x = screenWidth + gap;
                y = random.nextInt(screenHeight + gap) - gap/2;
            }
        }
        else {
            if (y > 0) {
                y = -gap;
                x = random.nextInt(screenWidth + gap) - gap/2;
            }
            else {
                y = screenHeight + gap;
                x = random.nextInt(screenWidth + gap) - gap /2;
            }
        }
    }
}
