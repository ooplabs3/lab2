package ru.nsu.ccfit.sukhinsky.gui;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;

import java.awt.*;

public class EarthView {
    private int x;
    private int y;
    private ShipData shipData;
    int height;
    int width;
    private int screenHeight;
    private int screenWidth;
    private boolean invisible;
    private Color color;

    public EarthView(ShipData shipData, int screenWidth, int screenHeight) {
        this.shipData = shipData;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        x = 0;
        y = 2 * screenHeight;
        height = screenHeight * 2 /3;
        width = screenWidth;
        invisible = true;
        color = new Color(128, 18, 98);
    }
    public void draw(Graphics g) {
        recountPosition();
        if (!invisible) {
            g.setColor(color);
            g.fillRect(x, y, width, height);
        }
    }

    private void recountPosition() {
//        if ((shipData.getCoords().getY() - screenHeight / 120.0) > 0) {
//            invisible = true;
//        }
//        else {
//            invisible = false;
//            y += shipData.getSpeed().getSpeedY();
//        }
        if (shipData.getCoords().getY() <= 6) {
            invisible = false;
            y = (int)(screenHeight - 60 * (6 - shipData.getCoords().getY()));
        }
        else {
            invisible = true;
        }
    }
}
