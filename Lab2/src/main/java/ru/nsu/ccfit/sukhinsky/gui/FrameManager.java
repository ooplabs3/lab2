package ru.nsu.ccfit.sukhinsky.gui;

import ru.nsu.ccfit.sukhinsky.ModelPublisher;
import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;
import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

public class FrameManager extends JFrame implements ShipManager.Subscriber {
    private MenuPanel menuPanel;
    private GamePanel gamePanel;
    private ShipData shipData;
    private ModelPublisher publisher;

    public void render() {

        gamePanel.render();
    }

    public void newGame(KeyListener keyListener) {

        menuPanel.setVisible(false);
        gamePanel.setVisible(true);
        add(gamePanel);
        setFocusable(true);
        requestFocusInWindow();
        addKeyListener(keyListener);
    }

    public void menu() {

        gamePanel.setVisible(false);
        menuPanel.setVisible(true);
        add(menuPanel);
    }

    public FrameManager(ModelPublisher publisher, ShipData shipData, KeyListener keyListener, ActionListener newGameActionListener, ActionListener exitActionListener) {
        this.shipData = shipData;
        this.publisher = publisher;
        publisher.subscribeLanded(this);
        publisher.subscribeUpdate(this);
        int height = 720;
        int width = 1280;

        //setResizable(false);
        setVisible(true);
        setTitle("Chaotic Lander");

        setSize(width, height);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        gamePanel = new GamePanel(shipData, width, height);
        gamePanel.addKeyListener(keyListener);
        menuPanel = new MenuPanel(newGameActionListener, exitActionListener);
        menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.Y_AXIS));
        menuPanel.setBorder(new EmptyBorder(new Insets(height/3, width/3, height/3, width/3)));
        menuPanel.setAlignmentX(SwingConstants.CENTER);
        menu();
    }
    private void landed() {
        if (shipData.isCrushed()) {
            JOptionPane.showMessageDialog(this, "Sorry, but you crushed. Next try would be lucky");
        }
        else {
            JOptionPane.showMessageDialog(this, "YEY! YOU DID IT! Now you are The Spaceship Captain");
        }
        menu();
    }
    @Override
    public void notify(ShipManager.ModelEvent event) {
        switch (event) {
            case UPDATE:
                render();
                break;
            case LANDED:
                landed();
                break;
            default:
                break;
        }
    }
}
