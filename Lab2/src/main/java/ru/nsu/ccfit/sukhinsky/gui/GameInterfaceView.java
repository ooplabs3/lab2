package ru.nsu.ccfit.sukhinsky.gui;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;
import ru.nsu.ccfit.sukhinsky.SpeedometerView;

import java.awt.*;

public class GameInterfaceView {
    private int screenHeight;
    private int screenWidth;
    private ShipData shipData;
    private HeightMeterView heightMeterView;
    private HeatMeterView heatMeterView;
    private SpeedometerView speedometerView;


    public void draw(Graphics g) {
        heightMeterView.draw(g);
        heatMeterView.draw(g);
        speedometerView.draw(g);
    }

    public GameInterfaceView(int screenHeight, int screenWidth, ShipData shipData) {
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        this.shipData = shipData;

        heightMeterView = new HeightMeterView(shipData, screenWidth / 20, 2 * screenHeight / 3, screenHeight, screenWidth);
        heatMeterView = new HeatMeterView(shipData,screenWidth/3, screenHeight/10, screenHeight, screenWidth);
        speedometerView = new SpeedometerView(shipData, 4 * screenWidth / 5, 4 * screenHeight / 5, screenHeight, screenWidth);
    }
}