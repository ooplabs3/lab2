package ru.nsu.ccfit.sukhinsky.gui;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class GamePanel extends JPanel {
   private BufferedImage gameScreenBuffer;
   private BufferedImage background;
   private Graphics2D gameGraphics;
   private ShipView shipView;
   private List<CloudView> clouds;
   private List<CloudView> frontClouds;
   private EarthView earth;
   private JLabel gameScreen;
   private GameInterfaceView gameInterfaceView;

   private ShipData shipData;

    public void render() {
        drawBackground(gameGraphics);

        for (CloudView cloud: clouds) {
            cloud.draw(gameGraphics);
        }

        earth.draw(gameGraphics);
        shipView.draw(gameGraphics);


        for (CloudView frontCloud: frontClouds) {
            frontCloud.draw(gameGraphics);
        }

        gameInterfaceView.draw(gameGraphics);

        gameScreen.setIcon(new ImageIcon(gameScreenBuffer));
    }
    public GamePanel(ShipData shipData, int width, int height) {
        this.shipData = shipData;
        background = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        initBackground();
        gameScreenBuffer = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        gameGraphics = gameScreenBuffer.createGraphics();
        shipView = new ShipView(shipData, width, height);
        gameScreen = new JLabel();
        clouds = new ArrayList<>();
        frontClouds = new ArrayList<>();

        ImageIcon cloudImage = new ImageIcon(GamePanel.class.getClassLoader().getResource("BigCloud.png"));
        for (int i = 0; i < 10; i++) {
            clouds.add(new CloudView(shipData, cloudImage, height, width, i));
        }
        for (int i = 0; i < 2; i++) {
            frontClouds.add(new CloudView(shipData, cloudImage, height, width, i + 10));
        }

        gameInterfaceView = new GameInterfaceView(height, width, shipData);

        earth = new EarthView(shipData, width, height);

        add(gameScreen);
    }

    private void drawBackground(Graphics g) {
        g.drawImage(background, 0, 0, null );
    }
    private void initBackground() {
        int smallWidth = background.getWidth() / 50;
        int smallHeight = background.getHeight() / 50;
        BufferedImage inputImage = new BufferedImage(smallWidth,smallHeight, BufferedImage.TYPE_4BYTE_ABGR);

        Graphics2D gin = inputImage.createGraphics();

        gin.setRenderingHint(RenderingHints.KEY_ANTIALIASING,               RenderingHints.VALUE_ANTIALIAS_OFF);
        gin.setRenderingHint(RenderingHints.KEY_INTERPOLATION,               RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        gin.setRenderingHint(RenderingHints.KEY_DITHERING,               RenderingHints.VALUE_DITHER_ENABLE);
        gin.setColor(new Color(200, 150, 50));
        GradientPaint gradient = new GradientPaint(smallWidth/2, 0, new Color(200, 150, 50), smallWidth / 2, smallHeight, new Color(250, 150, 140));
        gin.setPaint(gradient);

        gin.fillRect(0, 0, background.getWidth(), background.getHeight());

        Graphics2D g2d = background.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,               RenderingHints.VALUE_ANTIALIAS_OFF);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,               RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING,               RenderingHints.VALUE_DITHER_DISABLE);
        g2d.drawImage(inputImage, 0, 0, background.getWidth(), background.getHeight(), null);

    }
}
