package ru.nsu.ccfit.sukhinsky.gui;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;

import java.awt.*;

public class HeatMeterView {
    private ShipData shipData;
    private int x;
    private int y;
    private int screenHeight;
    private int screenWidth;
    private Color lowColor;
    private Color mediumColor;
    private Color highColor;
    private Color plainColor;
    private Color disableColor;
    private int height;
    private int width;
    private boolean blinkFlag;
    private int blinkCounter;
    private int strokeSize;
    private Stroke stroke;
    private Font font;
    private int fontSize;


    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(strokeSize));
        Color fillColor;
        Color textColor = plainColor;
        if (shipData.isEngineOverheated()) {
            fillColor = disableColor;
        }
        else if (shipData.getEngineHeat() < 0.3 * shipData.getMaximumHeat()) {
            fillColor = lowColor;
        } else if (shipData.getEngineHeat() < 0.7 * shipData.getMaximumHeat()) {
            fillColor = mediumColor;
        }
        else {
            if (blinkFlag) {
                fillColor = plainColor;
                textColor = highColor;
            }
            else {
                fillColor = highColor;
            }
            blinkCounter++;
            if (blinkCounter >= 30) {
                blinkFlag = !blinkFlag;
                blinkCounter = 0;
            }
        }

        g2.setColor(fillColor);
        g2.fillRect(x, y, width, height);


        String heatText = String.format("%.0f", shipData.getEngineHeat());
        FontMetrics fontMetrics = g2.getFontMetrics(font);
        int textX = x + width / 2 - fontMetrics.stringWidth(heatText) / 2;
        int textY = y + height / 2 + fontSize / 2;
        g2.setColor(textColor);
        g2.setFont(font);

        g2.drawString(heatText, textX, textY);
    }

    public HeatMeterView(ShipData shipData, int x, int y, int screenHeight, int screenWidth) {
        this.shipData = shipData;
        this.x = x;
        this.y = y;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;

        fontSize = screenHeight / 30;
        font = new Font(Font.SANS_SERIF, Font.BOLD, fontSize);

        height = screenHeight / 20;
        width = screenWidth / 3;

        strokeSize = 1;
        stroke = new BasicStroke(strokeSize);

        lowColor = new Color(50, 180, 110);
        mediumColor = new Color(220, 150, 20);
        highColor = new Color(255, 60, 10);
        disableColor = new Color(60, 60, 60);
        plainColor = new Color(255, 255, 255);
        blinkCounter = 0;

    }
}
