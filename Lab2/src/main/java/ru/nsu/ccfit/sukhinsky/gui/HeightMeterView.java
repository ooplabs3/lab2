package ru.nsu.ccfit.sukhinsky.gui;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;

import java.awt.*;

public class HeightMeterView {
    private int x;
    private int y;
    private Font shipHeightPositionTextFont;
    private int screenHeight;
    private int screenWidth;
    private int meterHeight;
    private int meterWidth;
    private int indicatorWidth;
    private int indicatorXOffset;
    private int indicatorRelativePosition;
    private double shipStartY;
    private ShipData shipData;
    private Stroke stroke;
    private int strokeSize;
    private Color color;

    public void draw(Graphics g) {
        drawMeter(g);
        drawIndicator(g);
    }

    private void drawMeter(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(color);
        g2.setStroke(new BasicStroke(10));
        g2.drawLine(x, y, x, y + meterHeight);
        g2.drawLine(x, y + meterHeight, x + meterWidth, y + meterHeight);
    }
    private void drawIndicator(Graphics g) {
        countIndicatorRelativePosition();
        g.drawLine(x + indicatorXOffset, y + indicatorRelativePosition, x + indicatorXOffset + indicatorWidth, y + indicatorRelativePosition);
        g.setFont(shipHeightPositionTextFont);
        int textX = x + indicatorXOffset + indicatorWidth + indicatorXOffset;
        int textY = y + indicatorRelativePosition + shipHeightPositionTextFont.getSize() / 2 - strokeSize / 2;
        g.drawString(String.format("%.0f", shipData.getCoords().getY()), textX, textY);
    }

    private void countIndicatorRelativePosition() {
        double shipHeightPositionPercentage =  shipData.getCoords().getY() / shipStartY;
        indicatorRelativePosition = meterHeight - strokeSize - (int)(shipHeightPositionPercentage * (meterHeight - strokeSize));
    }
    public HeightMeterView(ShipData shipData, int x, int y, int screenHeight, int screenWidth) {
        this.shipData = shipData;
        this.x = x;
        this.y = y;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        meterHeight = screenHeight / 5;
        meterWidth = screenWidth / 20;
        shipStartY = shipData.getCoords().getY();
        shipHeightPositionTextFont = new Font(Font.SANS_SERIF, Font.BOLD, screenWidth / 50);
        color = new Color(250, 180, 0);

        strokeSize = 10;
        stroke = new BasicStroke(strokeSize);
        indicatorXOffset = strokeSize * 2;
        indicatorWidth = 1;
    }

}
