package ru.nsu.ccfit.sukhinsky.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuPanel extends JPanel {
    private JLabel Logo;
    private JButton newGameButton;
    private JButton exitButton;

    public MenuPanel(ActionListener newGameActionListener, ActionListener exitActionListener) {
        newGameButton = new JButton("New game");
        newGameButton.addActionListener(newGameActionListener);
        newGameButton.setAlignmentX(CENTER_ALIGNMENT);

        exitButton = new JButton("Exit");
        exitButton.addActionListener(exitActionListener);
        exitButton.setAlignmentX(CENTER_ALIGNMENT);
        add(newGameButton);
        add(exitButton);
        //add(exitButton);
    }

}
