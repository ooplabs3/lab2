package ru.nsu.ccfit.sukhinsky.gui;

import ru.nsu.ccfit.sukhinsky.ShipLogic.ShipData;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class ShipView {
    private ImageIcon mainEngine;
    private ImageIcon ship;
    private ImageIcon rightRotator;
    private ImageIcon leftRotator;
    private int screenHeight;
    private int screenWidth;
    private ShipData shipData;

    private void rotateAndDraw(Graphics2D g2, List<ImageIcon> images, double angle) {
        double iconHeight = images.get(0).getIconHeight();
        double iconWidth = images.get(0).getIconWidth();
        double x = (screenWidth - iconWidth)/2.0;
        double y = (screenHeight - iconHeight)/2.0;
        AffineTransform at = AffineTransform.getTranslateInstance(x, y);
        at.rotate(Math.toRadians(angle), iconWidth/2.0, iconHeight/2.0);

        for (ImageIcon image: images) {
            g2.drawImage(image.getImage(), at, null);
        }

    }

    public void draw(Graphics g) {
        BufferedImage image;
        List<ImageIcon> imageParts = new ArrayList<>();

        if (!shipData.isEngineOverheated() && shipData.isEngineOn()) {
            imageParts.add(mainEngine);
        }

        imageParts.add(ship);
        switch (shipData.getRotation()) {
            case RIGHT:
                imageParts.add(leftRotator);
                break;
            case LEFT:
                imageParts.add(rightRotator);
                break;
            default:
                break;
        }

        image = new BufferedImage(imageParts.get(0).getIconWidth(), imageParts.get(0).getIconHeight(), BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = (Graphics2D) g.create();
        rotateAndDraw(g2, imageParts, 90 - shipData.getAngle());
        g2.drawImage(image, 10, 10, null);
        g2.dispose();
    }
    public ShipView(ShipData shipData, int screenWidth, int screenHeight) {
        this.shipData = shipData;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        this.ship =  new ImageIcon(GamePanel.class.getClassLoader().getResource("Ship.png"));
        this.rightRotator =  new ImageIcon(GamePanel.class.getClassLoader().getResource("RightFire.gif"));
        this.leftRotator =  new ImageIcon(GamePanel.class.getClassLoader().getResource("LeftFire.gif"));
        this.mainEngine =  new ImageIcon(GamePanel.class.getClassLoader().getResource("MainFire.gif"));
    }
}
